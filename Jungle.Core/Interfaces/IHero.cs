﻿using System.Collections.Generic;

namespace Jungle.Interfaces
{
    /// <summary>
    /// Interface of game hero.
    /// </summary>
    public interface IHero
    {
        /// <summary>
        /// Race of hero.
        /// </summary>
        string Race { get; }
        /// <summary>
        /// Hero's life level.
        /// </summary>
        byte Life { get; }
        
        /// <summary>
        /// Hero's weapons.
        /// </summary>
        IEnumerable<IWeapon> Weapons { get; }
        
        /// <summary>
        /// Attack.
        /// </summary>
        /// <param name="defender">Defender</param>
        void Attack(IHero defender);

        /// <summary>
        /// Defend.
        /// </summary>
        /// <param name="attackWeapon">Weapon to attack.</param>
        void Defend(IWeapon attackWeapon);

    }
}
