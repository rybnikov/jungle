﻿namespace Jungle.Races
{
    /// <summary>
    /// Sheep. Life level 100. Has no weapons.
    /// </summary>
    internal class Sheep : BaseHero
    {
        #region Constants
        /// <summary>
        /// Sheep's race name.
        /// </summary>
        internal const string SheepRaceName = "Sheep";
        /// <summary>
        /// Default sheep life level.
        /// </summary>
        internal const byte SheepLife = 100;
        #endregion

        #region Ctor's
        internal Sheep(byte life)
        {
            _life = life;
        }

        internal Sheep()
        {
            _life = SheepLife;
        }
        #endregion

        #region Impl
        protected override string RaceImpl()
        {
            return SheepRaceName;
        }
        #endregion
    }
}
