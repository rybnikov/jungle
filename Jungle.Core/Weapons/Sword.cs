﻿using Jungle.Interfaces;

namespace Jungle.Weapons
{
    /// <summary>
    /// Ork's Sword. Kill power 45.
    /// </summary>
    public class Sword : IWeapon
    {
        /// <summary>
        /// Kill power.
        /// </summary>
        public byte KillPower
        {
            get { return 45; }
        }

        internal Sword()
        {

        }
    }
}
