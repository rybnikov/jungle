﻿using System.Diagnostics;
using System.Diagnostics.Contracts;
using Jungle.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Jungle.Races
{
    /// <summary>
    /// Base hero.
    /// </summary>
    [DebuggerDisplay("Heor's race: {Race}; Life level:{Life}")]
    internal abstract class BaseHero : IHero
    {
        #region Fileds
        /// <summary>
        /// Heor's life level.
        /// </summary>
        protected byte _life = 0;
        /// <summary>
        /// Оружие
        /// </summary>
        protected IEnumerable<IWeapon> _weapons = Enumerable.Empty<IWeapon>(); 
        #endregion

        #region IHero impl
        public string Race { get { return RaceImpl(); } }

        public byte Life
        {
            get { return _life; }
        }

        public IEnumerable<IWeapon> Weapons
        {
            get { return _weapons; }
        }

        public void Defend(IWeapon atackWeapon)
        {
            DefendImpl(atackWeapon);
        }

        public void Attack(IHero defender)
        {
            AtackImpl(defender);
        } 
        #endregion

        #region Base logic.
        /// <summary>
        /// Base attack logic.
        /// </summary>
        /// <param name="defender">Defender.</param>
        protected virtual void AtackImpl(IHero defender)
        {
            defender.Defend(Weapons.OrderByDescending(weapon => weapon.KillPower).FirstOrDefault());
        }
        /// <summary>
        /// Base defend logic.
        /// </summary>
        /// <param name="attackWeapon">Weapon to fight.</param>
        protected virtual void DefendImpl(IWeapon attackWeapon)
        {
            Contract.Assert(attackWeapon != null);
            _life -= attackWeapon.KillPower;
        }
        protected abstract string RaceImpl();
       
        #endregion
       
    }
}
