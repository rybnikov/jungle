﻿using Jungle.Interfaces;
using Jungle.Weapons;
using System.Collections.Generic;

namespace Jungle.Races
{
    /// <summary>
    /// Ork. Default life level 200. Has sword.
    /// </summary>
    internal class Ork : BaseHero
    {
        #region Constants
        /// <summary>
        /// Ork's life level by default.
        /// </summary>
        internal const byte OrkLife = 200;
        /// <summary>
        /// Ork's race name.
        /// </summary>
        internal const string OrkRaceName = "Ork"; 
        #endregion
        
        #region Ctor's
        internal Ork(IEnumerable<IWeapon> weapon, byte life = OrkLife)
        {
            _life = life;
            _weapons = weapon;
        }
        internal Ork(byte life)
        {
            Init(life);
        }

        internal Ork() 
        {
            Init(OrkLife);
        }
        #endregion

        #region Impl
        private void Init(byte life)
        {
            _life = life;
            _weapons = new []
            {
                new Sword()
            };
        }
        protected override string RaceImpl()
        {
            return OrkRaceName;
        } 
        #endregion
    }
}
