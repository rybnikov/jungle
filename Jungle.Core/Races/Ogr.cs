﻿using Jungle.Interfaces;
using Jungle.Weapons;
using System.Collections.Generic;
using System.Linq;

namespace Jungle.Races
{
    /// <summary>
    /// Ogr. Life level 150. By default have Club.
    /// </summary>
    internal class Ogr : BaseHero
    {
        #region Constants
        /// <summary>
        /// Default Ogr's life level.
        /// </summary>
        internal const byte OgrLife = 150;
        /// <summary>
        /// Ogr's race name.
        /// </summary>
        internal const string OgrRaceName = "Ogr";
        #endregion

        #region Ctor's
        internal Ogr(IEnumerable<IWeapon> weapon, byte life = OgrLife)
        {
            _life = life;
            _weapons = weapon.ToList();
        }
        internal Ogr(byte life)
        {
            Init(life);
        }

        internal Ogr()
        {
            Init(OgrLife);
        }

        #endregion

        #region Logic
        private void Init(byte life)
        {
            _life = life;
            _weapons = new []
            {
                new Club()
            };
        }
        protected override string RaceImpl()
        {
            return OgrRaceName;
        }
        #endregion
    }
}
