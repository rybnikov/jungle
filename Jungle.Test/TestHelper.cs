﻿using System;
using System.Linq;
using System.Reflection;
using Jungle.Engine;
using Jungle.Interfaces;
using Jungle.Races;

namespace Jungle.Test
{
    /// <summary>
    /// Class which hepls init tests.
    /// </summary>
    internal static class TestHelper
    {
        /// <summary>
        /// Create band.
        /// </summary>
        /// <param name="num">Members count.</param>
        /// <param name="life">Members life level.</param>
        /// <typeparam name="T">Hero type.</typeparam>
        /// <returns></returns>
        public static Band CreateBand<T>(int num = 10, byte life = 100)
        {
            var group = new Band(String.Format("Only {0} group", typeof(T).Name));
            for (var i = 0; i < num; i++)
            {
                group.AddHero((IHero)Activator.CreateInstance(typeof(T), BindingFlags.Instance | BindingFlags.NonPublic, null, new object[] { life }, null));
            }
            return group;
        }

        /// <summary>
        /// Create band in current game.
        /// </summary>
        /// <param name="game">Current game.</param>
        /// <param name="num">Members count</param>
        /// <typeparam name="T">Hero type.</typeparam>
        /// <returns></returns>
        public static Band CreateGroup<T>(Game game, int num = 10) where T: IHero
        {
            var group = new Band(String.Format("Only {0} group", typeof(T).Name));
            for (var i = 0; i < num; i++)
            {
                group.AddHero(game.HeroesFactory.CreateHero<T>());
            }
            return group;
        }

        /// <summary>
        /// Create two bands and init focus.
        /// </summary>
        /// <param name="aggressor">Aggressors.</param>
        /// <param name="defender">Defenders.</param>
        /// <param name="focus">Focus manager.</param>
        public static void InitTwoBandsAndInitFocus<T1,T2>(out Band aggressor, out Band defender, out FocusManager focus)
        {
            aggressor = CreateBand<T1>();
            defender = CreateBand<T2>();
            focus = new FocusManager();
            focus.InitNewFight(aggressor, defender);
        }
        /// <summary>
        /// Fill group with only dead heroes.
        /// </summary>
        /// <param name="band">Band.</param>
        /// <param name="deadHeroCount">Dead heroes count.</param>
        public static void ClearGroupAndFillWithAllDeadHeroes(Band @band, int deadHeroCount = 30)
        {
            if (@band.Heroes.Any())
            {
                var heroesToRemove = @band.Heroes.ToArray();
                foreach (var hero in heroesToRemove)
                {
                    @band.RemoveHero(hero);
                }
            }
            InitGroupWithRandom(@band, deadHeroCount, 0);
        }

        /// <summary>
        /// Init band with random heroes
        /// </summary>
        /// <param name="band">Band.</param>
        /// <param name="heroCount">Heroes count.</param>
        /// <param name="life">Heroes life's level.</param>

        public static void InitGroupWithRandom(Band @band, int heroCount, byte life)
        {
            var randomizer = new Random();
            for (var i = 0; i < heroCount; i++)
            {
                switch (randomizer.Next(0, 2))
                {
                    case 0:
                        @band.AddHero(new Ogr(life));
                        break;
                    case 1:
                        @band.AddHero(new Ork(life));
                        break;
                    case 2:
                        @band.AddHero(new Sheep(life));
                        break;
                }
            }
        }
    }
}
