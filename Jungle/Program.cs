﻿using System;
using Jungle.Engine;
using Jungle.Weapons;

namespace Jungle
{
    internal class Program
    {
        private static readonly Game Game = new Game();

        private static void Main(string[] args)
        {
            DrawLineWithText("Starting game...");

            OrkEatSheep();

            OrksFightForShip();

            OgrsEatOrks();

            TwoOgrsTryKillOrkWithMagicSword();

            OrksAndShipsFightWithTwoOgrs();

            DrawLineWithText("The End.");
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }

        /// <summary>
        /// Flock of sheep with Band of Goblins fight with two Ogrs.
        /// </summary>
        private static void OrksAndShipsFightWithTwoOgrs()
        {
            DrawLine();
            Console.WriteLine("Flock of sheep with Band of Ork fight with two Ogrs.");
            var orksWithSheeps = new Band("Orks and sheeps", new[]
            {
                Game.HeroesFactory.CreateOrk(),
                Game.HeroesFactory.CreateSheep(),
                Game.HeroesFactory.CreateOrk(),
                Game.HeroesFactory.CreateSheep(),
                Game.HeroesFactory.CreateOrk(),
                Game.HeroesFactory.CreateSheep(),
                Game.HeroesFactory.CreateOrk(),
                Game.HeroesFactory.CreateSheep(),
                Game.HeroesFactory.CreateSheep()
            });
            var ogrs = new Band("Ogrs", new[]
            {
                Game.HeroesFactory.CreateOrk(),
                Game.HeroesFactory.CreateOrk()
            });
            var winer = Game.StartFight(orksWithSheeps, ogrs);
            Console.WriteLine("{0} wins!!!", winer.Name);
        }

        /// <summary>
        /// Two Ogrs fight with Goblin.
        /// </summary>
        private static void TwoOgrsTryKillOrkWithMagicSword()
        {
            DrawLine();
            Console.WriteLine("Two Ogrs fight with Ork which has The Magic Sword.");
            var ogrs = new Band("Ogrs", new[]
            {
                Game.HeroesFactory.CreateOgr(),
                Game.HeroesFactory.CreateOgr()
            });
            var orks = new Band("Ork with The Magic Sword", new[]
            {
                Game.HeroesFactory.CreateOrk(new[] { new MagicSword() })
            });
            var winer = Game.StartFight(ogrs, orks);
            Console.WriteLine("{0} - wins!!!", winer.Name);
        }

        /// <summary>
        /// Ogrs fight with Orks.
        /// </summary>
        private static void OgrsEatOrks()
        {
            DrawLine();
            Console.WriteLine("Ogrs fight with Orks.");
            var ogrs = new Band("Ogrs", new[]
            {
                Game.HeroesFactory.CreateOgr(),
                Game.HeroesFactory.CreateOgr(),
                Game.HeroesFactory.CreateOgr(),
                Game.HeroesFactory.CreateOgr(),
                Game.HeroesFactory.CreateOgr()
            });
            var orks = new Band("Orks", new[]
            {
                Game.HeroesFactory.CreateOrk(),
                Game.HeroesFactory.CreateOrk()
            });
            Band winer = Game.StartFight(ogrs, orks);
            Console.WriteLine("{0} wins!!!", winer.Name);
        }

        /// <summary>
        /// Ork fight with sheep
        /// </summary>
        private static void OrkEatSheep()
        {
            DrawLine();
            var orkGroup = new Band("Ork", new[] { Game.HeroesFactory.CreateOrk() });
            var sheepGroup = new Band("Sheep", new[] { Game.HeroesFactory.CreateSheep() });
            Console.WriteLine("Orks fight with sheep");
            var winer = Game.StartFight(orkGroup, sheepGroup);
            Console.WriteLine("{0} wins!!!!", winer.Name);
            DrawLine();
        }

        /// <summary>
        ///  Two Orks fight for sheep.
        /// </summary>
        private static void OrksFightForShip()
        {
            DrawLine();
            var firstOrk = new Band("Ork 1", new[] { Game.HeroesFactory.CreateOrk() });
            var secondOrk = new Band("Ork 2", new[] { Game.HeroesFactory.CreateOrk() });

            Console.WriteLine("Two Orks fight for sheep.");
            var winer = Game.StartFight(firstOrk, secondOrk);


            var sheepGroup = new Band("Sheep", new[] { Game.HeroesFactory.CreateSheep() });
            Console.WriteLine("{0} wins and eat sheep", winer.Name);
            Game.StartFight(winer, sheepGroup);

            DrawLine();
        }

        private static void DrawLineWithText(String text, char symbol = '=', int symbols = 60)
        {
            symbols = CalculateSymbols(text, symbols);
            DrawLine(false, symbol, symbols);
            Console.Write(" {0} ", text);
            DrawLine(false, symbol, symbols);
            Console.WriteLine();
        }

        private static int CalculateSymbols(string text, int symbols)
        {
            return symbols / 2 - ((text.Length + 2) / 2);
        }

        private static void DrawLine(bool lineBreak = true, char symbol = '=', int symbols = 60)
        {
            for (var i = 0; i < symbols; i++)
            {
                Console.Write(symbol);
            }
            if (lineBreak) Console.WriteLine();
        }
    }
}