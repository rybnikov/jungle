﻿using Jungle.Interfaces;
using Jungle.Races;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jungle.Test
{
    [TestClass]
    public class TestGame
    {
        private readonly Game _game = new Game();
     
        [TestMethod]
        public void TestAllCanEatSheeps()
        {
            TwoMonoGroupFight<Ork, Sheep>(_game);
            TwoMonoGroupFight<Ogr, Sheep>(_game);
        }
    
        [TestMethod]
        public void OrkKillOgr()
        {
            TwoMonoGroupFight<Ork, Ogr>(_game, false, 1, 1);
        }

        [TestMethod]
        public void TwoOgrsKillOrk()
        {
            TwoMonoGroupFight<Ogr, Ork>(_game, true, 8, 1);
        }

        [TestMethod]
        public void TwoOgrsFailToKill8Orks()
        {
            TwoMonoGroupFight<Ork, Ogr>(_game, false, 8, 1);
        }

        static void TwoMonoGroupFight<T1, T2>(Game game, bool firstIsWiner = true, int firstHeroesNum = 2, int secondHeroesNum = 2)  
                                     where T1 : IHero
                                     where T2 : IHero
        {
            var firsrtGroup = TestHelper.CreateGroup<T1>(game, firstHeroesNum);
            var secondGroup = TestHelper.CreateGroup<T2>(game, secondHeroesNum);

            var winer = game.StartFight(firsrtGroup, secondGroup);

            Assert.AreEqual(firstIsWiner ? firsrtGroup : secondGroup, winer);
        }
    }
}
