﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Jungle.Interfaces;

namespace Jungle.Engine
{
    /// <summary>
    /// Group
    /// </summary>
    [DebuggerDisplay("Band name :{Name}; Count:{Heroes}; Has alive heroes:{HasLiveHeroes};}")]
    public class Band
    {
        #region Fields
        
        private readonly List<IHero> _heroes; 
        
        #endregion

        #region Свойства
        /// <summary>
        /// Grup name
        /// </summary>
        public String Name { get; private set; }
        
        /// <summary>
        /// Group members
        /// </summary>
        public IEnumerable<IHero> Heroes
        {
            get { return _heroes; }
        }

        /// <summary>
        /// Are any alive heroes in this band?
        /// </summary>
        public bool HasAliveHeroes
        {
             get { return _heroes.Any(hero => hero.Life > 0); }
        }

        /// <summary>
        /// Has this band heroes with weapon?
        /// </summary>
        public bool HasHeroesWithWeapon
        {
            get { return _heroes.Any(hero => hero.Life > 0 && hero.Weapons.Any(weapon => weapon.KillPower > 0)); }
        }

        /// <summary>
        /// Which heroe has max kill power?
        /// </summary>
        public IHero HeroWithMaxKillPower
        {
            get
            {
                if (!HasHeroesWithWeapon)
                {
                    return null;
                }

                return _heroes.Where(hero => hero.Weapons.Any() && hero.Life > 0)
                              .Select(hero => new Tuple<IHero, byte>(hero, hero.Weapons.Max(weapon => weapon.KillPower)))
                              .OrderByDescending(res => res.Item2)
                              .First().Item1;
            }
        }
        /// <summary>
        /// Which hero has maximum life?
        /// </summary>
        public IHero HeroWithMaxLife
        {
            get
            {
                return !HasAliveHeroes ? null : _heroes.Where(hero => hero.Life > 0).OrderByDescending(hero => hero.Life).First();
            }
        }
        #endregion

        #region Ctor's

        /// <summary>
        /// Band with set of heroes.
        /// </summary>
        /// <param name="name">Band name</param>
        /// <param name="heroes">Set</param>
        public Band(String name, IEnumerable<IHero> heroes)
        {
            Name = name;
            _heroes = heroes.ToList();
        }

        /// <summary>
        /// Empty band with name
        /// </summary>
        /// <param name="name">Group name</param>
        public Band(String name)
        {
            Name = name;
            _heroes = new List<IHero>();
        }

        #endregion

        #region Logic
        /// <summary>
        /// Add hero to band.
        /// </summary>
        /// <param name="hero">Hero to add</param>
        public void AddHero(IHero hero)
        {
            _heroes.Add(hero);
        }

        /// <summary>
        /// Remove hero.
        /// </summary>
        /// <param name="hero">Hero to add</param>
        public void RemoveHero(IHero hero)
        {
            _heroes.Remove(hero);
        }

        #endregion
    }
}
