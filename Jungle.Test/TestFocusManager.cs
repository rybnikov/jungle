﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jungle.Engine;
using Jungle.Interfaces;
using Jungle.Races;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jungle.Test
{
    [TestClass]
    public class TestFocusManager
    {
        /// <summary>
        /// Группа агресора
        /// </summary>
        private Band _aggressors;
        /// <summary>
        /// Группа защищающихся
        /// </summary>
        private Band _defenders;
        /// <summary>
        /// Менеджер управления фокусом
        /// </summary>
        private FocusManager _focus;

        public TestFocusManager()
        {
            TestHelper.InitTwoBandsAndInitFocus<Ork, Ogr>(out _aggressors, out _defenders, out _focus);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "Sequence of steps is invalid.")]
        public void DuplicateNextAgressorCall()
        {
            _focus.GetNextAggressor();
            _focus.GetNextAggressor();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "Sequence of steps is invalid.")]
        public void DuplicateDefenderCall()
        {
            _focus.GetNextAggressor();
            _focus.GetNextDefender();
            _focus.GetNextDefender();
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "Sequence of steps is invalid.")]
        public void InvalidNextHeroCallSequence()
        {
            _focus.GetNextDefender();
            _focus.GetNextAggressor();
        }

        [TestMethod]
        public void FocusOnOddAndEvenStep()
        {
            TestFocus();
        }


        [TestMethod]
        public void FocusOnGroupWithHeroesWithoutWeapon()
        {
            _focus.EndCurrentFight();
            TestHelper.InitTwoBandsAndInitFocus<Ork, Sheep>(out _aggressors, out _defenders, out _focus);
            TestFocus();
        }

        [TestMethod]
        public void PartOfAggressorsDieInFight()
        {
            InitTestFightForHeroesDieInFight();

            TestHelper.ClearGroupAndFillWithAllDeadHeroes(_aggressors);
            var testOrk = new Ork();
            _aggressors.AddHero(testOrk);

            Assert.AreEqual(testOrk, _focus.GetNextAggressor());
        }

        [TestMethod]
        public void PartOfDefendersDieInFight()
        {
            InitTestFightForHeroesDieInFight();

            TestHelper.ClearGroupAndFillWithAllDeadHeroes(_defenders);
            var testOrk = new Ork();
            _defenders.AddHero(testOrk);
            _focus.GetNextAggressor();

            Assert.AreEqual(testOrk, _focus.GetNextDefender());
        }


        [TestMethod]
        public void AggressorWins()
        {
            Assert.IsTrue(_aggressors.Heroes.Contains(_focus.GetNextAggressor()));
            TestHelper.ClearGroupAndFillWithAllDeadHeroes(_defenders);
            Assert.IsNull(_focus.GetNextDefender());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "Invalid fight initialization")]
        public void InvalidFightInicialization()
        {
            var focus = new FocusManager();
            var groupA = TestHelper.CreateBand<Ork>();
            var groupB = TestHelper.CreateBand<Ogr>();
            focus.InitNewFight(groupA, groupB);
            focus.InitNewFight(groupA, groupB);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException), "Invalid fight initialization")]
        public void InvalidFightEnding()
        {
            var focus = new FocusManager();
            focus.EndCurrentFight();
        }


        private void InitTestFightForHeroesDieInFight()
        {
            _focus.EndCurrentFight();
            TestHelper.InitTwoBandsAndInitFocus<Ork, Ogr>(out _aggressors, out _defenders, out _focus);
            //First step
            _focus.GetNextAggressor();
            _focus.GetNextDefender();
            //Second step
            _focus.GetNextAggressor();
            _focus.GetNextDefender();
        }

        private void TestFocus()
        {
            for (var i = 1; i <= 10; i++)
            {
                var aggressonHero = _focus.GetNextAggressor();
                var defendHero = _focus.GetNextDefender();
                if (i % 2 != 0)
                {
                    CheckAggressorAndDefenderFocus(_aggressors, aggressonHero, _defenders, defendHero, _aggressors.HasHeroesWithWeapon, i);
                }
                else
                {
                    CheckAggressorAndDefenderFocus(_defenders, aggressonHero, _aggressors, defendHero, _defenders.HasHeroesWithWeapon, i);
                }
                if (!_aggressors.HasAliveHeroes || !_defenders.HasAliveHeroes)
                {
                    break;
                }
            }
        }

        private static void CheckAggressorAndDefenderFocus(Band agressors, IHero aggressonHero, Band defenders, IHero defendHero, bool direct, int step)
        {
            if (direct)
            {
                CheckAggressorAndDefenderFocus(agressors, aggressonHero, defenders, defendHero, step);
            }
            else
            {
                CheckAggressorAndDefenderFocus(agressors, defendHero, agressors, defendHero, step);
            }
        }

        private static void CheckAggressorAndDefenderFocus(Band agressors, IHero aggressonHero, Band defenders, IHero defendHero, int step)
        {
            Assert.IsTrue(agressors.Heroes.Contains(aggressonHero), String.Format("Error at step {0}", step));
            Assert.IsTrue(defenders.Heroes.Contains(defendHero), String.Format("Error at step: {0}", step));
        }

    }
}
