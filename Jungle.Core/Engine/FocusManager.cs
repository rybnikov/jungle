﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Jungle.Interfaces;

namespace Jungle.Engine
{
    /// <summary>
    /// Focus manager for N groups.
    /// </summary>
    internal sealed class FocusManager
    {
        #region Fields
        /// <summary>
        /// Groups that will fight
        /// </summary>
        private readonly List<Band> _groups = new List<Band>(2);
        
        /// <summary>
        /// Active agressors
        /// </summary>
        private int _activeAggressorGroupIndex;
        /// <summary>
        /// Active defender
        /// </summary>
        private int _activeDefenderGroupIndex;

        /// <summary>
        /// Step counter.
        /// </summary>
        private int _gameStepCounter;

        /// <summary>
        /// Init counter
        /// </summary>
        private int _fightsCounter;

        #endregion

        #region Ctor's
        internal FocusManager()
        {

        } 
        #endregion

        #region Logic

        /// <summary>
        /// Init new fight
        /// </summary>
        /// <param name="aggressors">Группа агресоров</param>
        /// <param name="defenders">Группа защищающихся</param>
        internal void InitNewFight(Band aggressors, Band defenders)
        {
            Contract.Assert(aggressors != null, "Aggressors can't be null");
            Contract.Assert(defenders != null, "Defenders can't be null");

            Contract.Assert(aggressors.Heroes.Any(hero => hero.Life > 0 && hero.Weapons.Any( weapon => weapon.KillPower > 0)), 
                                                 "Agressors can't be empty and all must be alive. Some one of them must have weapon");

            Contract.Assert(defenders.Heroes.Any(hero => hero.Life > 0), "Defenders can't be empty and all must be alive");

            if (_fightsCounter != 0)
            {
                throw new InvalidOperationException("Invalid fight initialization");
            }
            _fightsCounter++;

            _groups.Add(aggressors);
            _groups.Add(defenders);
            _activeAggressorGroupIndex = -1;
            _activeDefenderGroupIndex = -1;
        }
        /// <summary>
        /// Stop fight
        /// </summary>
        internal void EndCurrentFight()
        {
            if (_fightsCounter == 0)
            {
                throw new InvalidOperationException("Invalid fight initialization");
            }
            _fightsCounter--;
            _groups.Clear();
        }

        /// <summary>
        /// Get next aggressor.
        /// </summary>
        /// <returns>Next aggressor. Return null if nobody can get focus.</returns>
        internal IHero GetNextAggressor()
        {
            Contract.Assert(_groups.Any(group => group.HasHeroesWithWeapon));
            if (_gameStepCounter != 0)
            {
                throw new InvalidOperationException("Sequence of steps is invalid.");
            }
            _gameStepCounter++;
            IHero nextHero;
            do
            {
                var nextGroupNum = GetNextAggressorGroupIndex();
                nextHero = nextGroupNum == -1 ? null : _groups[nextGroupNum].HeroWithMaxKillPower;
            } while (nextHero == null);

            return nextHero;
        }
        /// <summary>
        /// Get next defender. 
        /// </summary>
        /// <returns>Next defender. Return null if nobody can get focus.</returns>
        internal IHero GetNextDefender()
        {
            if (_gameStepCounter != 1)
            {
                throw new InvalidOperationException("Sequence of steps is invalid");
            }
            _gameStepCounter--;
            var nextGroupIndex = GetNextDefenderGroupIndex();
            return nextGroupIndex == -1 ? null : _groups[nextGroupIndex].HeroWithMaxLife;
        }

        /// <summary>
        /// Get next aggressor group index. 
        /// </summary>
        /// <returns>Index of next focused group. Return -1 if nobody can get focus.</returns>
        private int GetNextAggressorGroupIndex()
        {
            _activeAggressorGroupIndex = FindNextIndex(_groups, _activeAggressorGroupIndex);
            return _activeAggressorGroupIndex;
        }
        /// <summary>
        /// Get next defender group index. 
        /// </summary>
        /// <returns>Index of next focused group. Return -1 if nobody can get focus.</returns>
        private int GetNextDefenderGroupIndex()
        {
            _activeDefenderGroupIndex = FindNextIndex(_groups, _activeAggressorGroupIndex);
            return _activeDefenderGroupIndex;
        }

        private static int FindNextIndex(ICollection<Band> groups, int startIndex)
        {
            var index = startIndex;
            var count = groups.Count;
            while(true) 
            {
                index++;
                if (index >= count)
                {
                    index = 0;
                }
                if(startIndex == index)
                {
                    return -1;
                }
                if (groups.ElementAt(index).HasAliveHeroes)
                {
                    return index;
                }
            } 
        }

        #endregion

    }
}
