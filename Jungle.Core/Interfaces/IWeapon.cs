﻿namespace Jungle.Interfaces
{
    /// <summary>
    /// Weapon interface.
    /// </summary>
    public interface IWeapon
    {
        /// <summary>
        /// Kill power.
        /// </summary>
        byte KillPower { get; }

    }
}
