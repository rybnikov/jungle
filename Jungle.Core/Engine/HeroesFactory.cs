﻿using System.Reflection;
using Jungle.Interfaces;
using Jungle.Races;
using System;
using System.Collections.Generic;

namespace Jungle.Engine
{
    /// <summary>
    /// Heroes factory
    /// </summary>
    public sealed class HeroesFactory
    {
        #region Fields
        /// <summary>
        /// Currnet game.
        /// </summary>
        private readonly Game _currentGame; 
        #endregion

        #region Ctor's
        /// <summary>
        /// Factory
        /// </summary>
        /// <param name="currentGame">Current game.</param>
        internal HeroesFactory(Game currentGame)
        {
            _currentGame = currentGame;
        } 
        #endregion

        #region Methods
        /// <summary>
        /// Create new Ork.
        /// </summary>
        /// <param name="life">Ork's life level.</param>
        /// <param name="weapons">Weapons.</param>
        /// <returns>Ork</returns>
        internal IHero CreateOrk(IEnumerable<IWeapon> weapons, byte life)
        {
            var newOrk = new Ork(weapons, life);
            _currentGame.AddHero(newOrk);
            return newOrk;
        }
        /// <summary>
        /// Create new Ork.
        /// </summary>
        /// <param name="life">Ork's life level.</param>
        /// <returns>Ork</returns>
        internal IHero CreateOrk(byte life)
        {
            var newOrk = new Ork(life);
            _currentGame.AddHero(newOrk);
            return newOrk;
        }

        /// <summary>
        /// Create new Ork.
        /// </summary>
        public IHero CreateOrk()
        {
            var newOrk = new Ork();
            _currentGame.AddHero(newOrk);
            return newOrk;
        }

        /// <summary>
        /// Create new Ork.
        /// </summary>
        /// <param name="weapons">Weapons.</param>
        /// <returns>Ork</returns>
        public IHero CreateOrk(IEnumerable<IWeapon> weapons)
        {
            var newOrk = new Ork(weapons);
            _currentGame.AddHero(newOrk);
            return newOrk;
        }

        /// <summary>
        /// Create new ogr.
        /// </summary>
        /// <param name="life">Ogr's life level</param>
        /// <param name="weapon">Weapons.</param>
        /// <returns>Ogr</returns>
        internal IHero CreateOgr(IEnumerable<IWeapon> weapon, byte life)
        {
            var newOgr = new Ogr(weapon, life);
            _currentGame.AddHero(newOgr);
            return newOgr;
        }

        /// <summary>
        /// Create new ogr.
        /// </summary>
        /// <param name="life">Ogr's life level</param>
        /// <returns>Ogr</returns>
        internal IHero CreateOgr(byte life)
        {
            var newOgr = new Ogr(life);
            _currentGame.AddHero(newOgr);
            return newOgr;
        }

        /// <summary>
        /// Create new ogr.
        /// </summary>
        /// <returns>Ogr</returns>
        public IHero CreateOgr()
        {
            var newOgr = new Ogr();
            _currentGame.AddHero(newOgr);
            return newOgr;
        }

        /// <summary>
        /// Create new ogr
        /// </summary>
        /// <param name="weapons">Weapons.</param>
        /// <returns>Ogr</returns>
        public IHero CreateOgr(IEnumerable<IWeapon> weapons)
        {
            var newOgr = new Ogr(weapons);
            _currentGame.AddHero(newOgr);
            return newOgr;
        }
        /// <summary>
        /// Create sheep.
        /// </summary>
        /// <returns></returns>
        public IHero CreateSheep()
        {
            var newSheep = new Sheep();
            _currentGame.AddHero(newSheep);
            return newSheep;
        }
        /// <summary>
        /// Create custom hero.
        /// </summary>
        /// <typeparam name="T">Hero type.</typeparam>
        /// <returns>New hero of type T</returns>
        internal IHero CreateHero<T>() where T : IHero
        {
            var hero = (IHero)Activator.CreateInstance(typeof(T), BindingFlags.Instance | BindingFlags.NonPublic, null, new object[] { }, null);
            _currentGame.AddHero(hero);
            return hero;
        }

        #endregion
    }
}
