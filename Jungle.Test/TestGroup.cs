﻿using System.Collections.Generic;
using System.Linq;
using Jungle.Engine;
using Jungle.Interfaces;
using Jungle.Races;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jungle.Test
{
    [TestClass]
    public class TestGroup
    {

        [TestMethod]
        public void AllHeroesAlive()
        {
            var group = new Band("test");
            TestHelper.InitGroupWithRandom(group, 30, 100);
            Assert.IsTrue(group.HasAliveHeroes);
        }

        [TestMethod]
        public void AllHeroesDead()
        {
            var group = new Band("test");
            TestHelper.InitGroupWithRandom(group, 30, 0);
            Assert.IsFalse(group.HasAliveHeroes);
        }
        

        [TestMethod]
        public void DeferentRacesAliveAndDead()
        {
            var group = new Band("Test group");
            
            group.AddHero(new Ogr()); 
            group.AddHero(new Ogr(0));
            group.AddHero(new Ogr()); 
            group.AddHero(new Ork());
            group.AddHero(new Ork(0));
            group.AddHero(new Ork());
            group.AddHero(new Sheep());
            group.AddHero(new Sheep(0));
            group.AddHero(new Sheep());

            Assert.AreEqual(group.HasAliveHeroes, true);
        }

        [TestMethod]
        public void OrkHasMaxKillPower()
        {
            var group = new Band("Test group");
            var testOrk = new Ork();
            group.AddHero(new Ogr());
            group.AddHero(testOrk);
            group.AddHero(new Sheep());
            
            Assert.AreSame(group.HeroWithMaxKillPower, testOrk);
        }
        

        [TestMethod]
        public void OgrHasMaxKillPower()
        {
            var group = new Band("Test group");
            var testOgr = new Ogr();
            group.AddHero(testOgr);
            group.AddHero(new Sheep());

            Assert.AreSame(group.HeroWithMaxKillPower, testOgr);
        }

      
        [TestMethod]
        public void OnlySheeps()
        {
            var group = new Band("Test group");
            for (int i = 0; i < 3; i++)
            {
                group.AddHero(new Sheep());
            }
            Assert.IsNull(group.HeroWithMaxKillPower, null);
        }

      
        [TestMethod]
        public void AliveHeroWithMaxPower()
        {
            var group = new Band("Test group");
            var testOrk = new Ork(0);
            var testOgr = new Ogr();
            group.AddHero(testOgr);
            group.AddHero(testOrk);
            group.AddHero(new Sheep());
            
            Assert.AreEqual(group.HeroWithMaxKillPower, testOgr);
        }
       
        [TestMethod]
        public void GroupHasAllAddHeroes()
        {
            var group = new Band("Test group");
            
            var testOrk = new Ork();
            var testOgr = new Ogr();
            var testSheep = new Sheep();
            
            var listOfHeroes = new List<IHero>(3){ testOgr, testOrk, testSheep};
            
            group.AddHero(testOgr);
            group.AddHero(testOrk);
            group.AddHero(testSheep);

            Assert.IsTrue(listOfHeroes.SequenceEqual(@group.Heroes));
        }


        [TestMethod]
        public void GroupHasNotContainsAllRemoveHeroes()
        {
            var group = new Band("Test group");

            var testOrk = new Ork();
            var testOgr = new Ogr();
            var testSheep = new Sheep();

            var listOfHeroes = new List<IHero>(3) { testOgr, testSheep };

            group.AddHero(testOgr);
            group.AddHero(testOrk);
            group.AddHero(testSheep);
            group.RemoveHero(testOrk);

            Assert.IsTrue(listOfHeroes.SequenceEqual(@group.Heroes));
        }


        [TestMethod]
        public void HasHeroesWithWeapons()
        {
            CheckGroupMonoType<Sheep>(false);
            CheckGroupMonoType<Ork>(true);
            CheckGroupMonoType<Ogr>(true);
        }

        [TestMethod]
        public void OnlyDeadHeroesHasWeapon()
        {
            var group = TestHelper.CreateBand<Ork>(10, 0);
            group.AddHero(new Sheep());
            Assert.IsFalse(group.HasHeroesWithWeapon);
        }

        static void CheckGroupMonoType<T>(bool result)
        {
            var group = TestHelper.CreateBand<T>();
            Assert.AreEqual(group.HasHeroesWithWeapon, result);
        }
    }
}
