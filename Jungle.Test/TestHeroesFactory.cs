﻿using System;
using System.Collections.Generic;
using System.Linq;
using Jungle.Interfaces;
using Jungle.Races;
using Jungle.Weapons;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Jungle.Test
{
    [TestClass]
    public class TestHeroesFactory
    {
        private readonly Game _game = new Game();
        
        private readonly IList<IWeapon> _testWeapons = new List<IWeapon> { new Club(), new Sword(), new MagicSword() };

       
        [TestMethod]
        public void FactoryCreateSheep()
        {
            var sheep = _game.HeroesFactory.CreateSheep();
            CheckHero<Sheep>(sheep, Sheep.SheepRaceName, Sheep.SheepLife, weapon => !weapon.Any());
        }
        
        [TestMethod]
        public void FactoryCreateOgr()
        {
            var ogr = _game.HeroesFactory.CreateOgr();

            CheckHero<Ogr>(ogr, Ogr.OgrRaceName, Ogr.OgrLife, CheckStandartWeapon<Club>);

            ogr = _game.HeroesFactory.CreateOgr(30);
            CheckHero<Ogr>(ogr, Ogr.OgrRaceName, 30, CheckStandartWeapon<Club>);

            ogr = _game.HeroesFactory.CreateOgr(_testWeapons);
            CheckHero<Ogr>(ogr, Ogr.OgrRaceName, Ogr.OgrLife, CheckTestWeapons);

            ogr = _game.HeroesFactory.CreateOgr(_testWeapons, 40);
            CheckHero<Ogr>(ogr, Ogr.OgrRaceName, 40, CheckTestWeapons);
        }

        
        [TestMethod]
        public void FactoryCreateOrk()
        {
            var ogr = _game.HeroesFactory.CreateOrk();
            CheckHero<Ork>(ogr, Ork.OrkRaceName, Ork.OrkLife, CheckStandartWeapon<Sword>);

            ogr = _game.HeroesFactory.CreateOrk(30);
            CheckHero<Ork>(ogr, Ork.OrkRaceName, 30, CheckStandartWeapon<Sword>);

            ogr = _game.HeroesFactory.CreateOrk(_testWeapons);
            CheckHero<Ork>(ogr, Ork.OrkRaceName, Ork.OrkLife, CheckTestWeapons);

            ogr = _game.HeroesFactory.CreateOrk(_testWeapons, 40);
            CheckHero<Ork>(ogr, Ork.OrkRaceName, 40, CheckTestWeapons);
        }
       
        [TestMethod]
        public void AllHeroesInGame()
        {
            var heroesList = new List<IHero>
            {
                _game.HeroesFactory.CreateOgr(),
                _game.HeroesFactory.CreateOgr(),
                _game.HeroesFactory.CreateOrk(),
                _game.HeroesFactory.CreateOrk(),
                _game.HeroesFactory.CreateSheep(),
                _game.HeroesFactory.CreateSheep()
            };
            Assert.IsTrue(_game.Heroes.SequenceEqual(heroesList));
        }
        
        static void CheckHero<T>(IHero hero, string raseName, byte life, Func<IEnumerable<IWeapon>, bool> validateWeapons)
        {
            Assert.IsTrue(hero is T);
            Assert.AreSame(hero.Race, raseName);
            Assert.AreEqual(hero.Life, life);
            Assert.IsTrue(validateWeapons(hero.Weapons));
        }

       
        static bool CheckStandartWeapon<T>(IEnumerable<IWeapon> weapons)
        {
            var localWeapons = weapons.ToArray();
            return localWeapons.Any() && localWeapons.Count() == 1 && localWeapons.First() is T;
        }

        
        static bool CheckTestWeapons(IEnumerable<IWeapon> weapons)
        {
            var localWeapons = weapons.ToArray();
            return localWeapons.Any() &&
                   localWeapons.Count() == 3 &&
                   localWeapons.Any(weapon => weapon is Club) &&
                   localWeapons.Any(weapon => weapon is Sword) &&
                   localWeapons.Any(weapon => weapon is MagicSword);
        }

    }
}
