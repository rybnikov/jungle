﻿using Jungle.Interfaces;

namespace Jungle.Weapons
{
    /// <summary>
    /// Club. Kill power 25.
    /// </summary>
    public class Club : IWeapon
    {
        /// <summary>
        /// Убойная сила
        /// </summary>
        public byte KillPower
        {
            get { return 25; }
        }
        internal Club()
        {

        }
    }
}
