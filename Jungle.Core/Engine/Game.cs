﻿using System.Diagnostics.Contracts;
using Jungle.Engine;
using Jungle.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Jungle
{
    /// <summary>
    /// Класс реализующий логику игры.
    /// </summary>
    public sealed class Game
    {
        #region Поля
        /// <summary>
        /// Управление фокусом
        /// </summary>
        readonly FocusManager _focus = new FocusManager();
        /// <summary>
        /// Участники игры
        /// </summary>
        private readonly HashSet<IHero> _heroes = new HashSet<IHero>();
        #endregion

        #region Свойства
        /// <summary>
        /// Герой зарегестрированые в игре
        /// </summary>
        internal IEnumerable<IHero> Heroes { get { return _heroes; } } 
        /// <summary>
        /// Фабрика героев
        /// </summary>
        public HeroesFactory HeroesFactory { get; private set; }
        #endregion

        #region Конструктры
        public Game()
        {
            HeroesFactory = new HeroesFactory(this);
        }
        #endregion

        #region Логика
        /// <summary>
        /// Начать сражание между двумя группами участников
        /// </summary>
        /// <param name="aggressors">Группа агресоров</param>
        /// <param name="defenders">Группа защищающихся</param>
        /// <returns>Победитель</returns>
        public Band StartFight(Band aggressors, Band defenders)
        {
            CheckGroup(aggressors);
            Contract.Assert(aggressors.Heroes.Any( hero => hero.Weapons.Any( weapon => weapon.KillPower > 0)), "У нападающих должно быть оружие");
            CheckGroup(defenders);

            _focus.InitNewFight(aggressors, defenders);

            try
            {
                do
                {
                    var nextAgressor = _focus.GetNextAggressor();
                    var nextDefender = _focus.GetNextDefender();
                    if (nextDefender == null)
                    {
                        break;
                    }
                    
                    nextAgressor.Attack(nextDefender);
                } while (aggressors.HasAliveHeroes || defenders.HasAliveHeroes);
            }
            finally
            {
                _focus.EndCurrentFight();
            }
            RemoveAllDeadHeroes(aggressors, defenders);
            return aggressors.HasAliveHeroes ? aggressors : defenders;
        }
        /// <summary>
        /// Удаляем всех убитых героев
        /// </summary>
        /// <param name="aggressors">Группа агрессоров</param>
        /// <param name="defenders">Группа защищающихся</param>
        private void RemoveAllDeadHeroes(Band aggressors, Band defenders)
        {
            foreach (var hero in aggressors.Heroes.Where(HeroIsDead).ToArray())
            {
                aggressors.RemoveHero(hero);
            }
            foreach (var hero in aggressors.Heroes.Where(HeroIsDead).ToArray())
            {
                aggressors.RemoveHero(hero);
            }
            _heroes.RemoveWhere(HeroIsDead);
        }

        static bool HeroIsDead(IHero hero)
        {
            return hero.Life <= 0;
        }

        /// <summary>
        /// Проверяем группу на валидность для игры.
        /// </summary>
        /// <param name="groupToCheck">Группа для проверки</param>
        /// <remarks>Проверяем что группа не пуста. Что в ней есть живые участники. Что все участники зарегистрированы в игре</remarks>
        private void CheckGroup(Band groupToCheck)
        {
            Contract.Assert(groupToCheck.Heroes.Any(), "Список группы пуст");
            Contract.Assert(groupToCheck.HasAliveHeroes, "У группы нет живых участников");
            Contract.Assert(_heroes.IsProperSupersetOf(groupToCheck.Heroes), "Не все участники группы зарегестрированы в игре");
        }
        /// <summary>
        /// Добавление участника в игру
        /// </summary>
        /// <param name="hero"></param>
        internal void AddHero(IHero hero)
        {
            //TODO:Потом надо бы добвать синхронизацию достпа к списку. 
            _heroes.Add(hero);
        }
        internal void RemoveHero(IHero hero)
        {
            //TODO:Добавить синхронизацию
            _heroes.Remove(hero);
        }
        #endregion
    }
}
