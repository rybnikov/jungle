﻿using Jungle.Interfaces;

namespace Jungle.Weapons
{
    /// <summary>
    /// The Magic Sword. Kill power 150.
    /// </summary>
    public class MagicSword : IWeapon
    {
        /// <summary>
        /// Kill power.
        /// </summary>
        public byte KillPower
        {
            get { return 150; }
        }

        public MagicSword()
        {

        }
    }
}
